#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 29 12:53:20 2019

@author: chen

for cs425 sp19 mp3
ip address of vm-01:  172.22.94.228


"""
import sys
import threading
import Pyro4
from threading import Lock

@Pyro4.expose
class Transaction(object):
    '''
    Class for Remote Method Invocation
    '''
    def __init__(self):
        #get local server information once connected
        self.accounts = accounts_server.copy()
        self.tentative_accounts = accounts_server.copy()
        
    def create_tent_account(self,name,value,client_id):
        #check lock
        #initiate lock if not exist
        if name not in locks:
            locks[name] = Account(name)
            
        #Condition 1: no read or write lock
        if (not locks[name].Rlock.locked()) and (not locks[name].Wlock.locked()):
            
            
            #acquire both write and read lock
            locks[name].Wlock.acquire()
            locks[name].Rlock.acquire()
            #record the client for the lock
            locks[name].Wlock_client = client_id
            locks[name].addClients_toRlock(client_id)
            #
            self.tentative_accounts[name] = value
            print(locks[name].Wlock_client)
            print(locks[name].Wlock.locked())
            return "OK"
        
        #Condition 2: only write no read:
        #ACTUALL this condition will not appear.
        if (locks[name].Wlock.locked()) and (not locks[name].Rlock.locked()):
            print('write lock is detected, a client {} is waiting'.format(client_id))
            #if locked by self:
            if locks[name].Wlock_client == client_id:
                print('Locked by self, OK')
                self.tentative_accounts[name] = value
                return "OK"
        
            else:
                #acquire write lock
                locks[name].Wlock.acquire() #waiting
                locks[name].Rlock.acquire()
                #change the client for the lock
                locks[name].Wlock_client = client_id
                locks[name].addClients_toRlock(client_id)
                self.tentative_accounts[name] = value
                return "OK"
        
        #condition 3: only read no write
        if (not locks[name].Wlock.locked()) and (locks[name].Rlock.locked()):
            # if read by self:
            if len(locks[name].Rlock_clients) ==1 and (client_id in locks[name].Rlock_clients):
                #acquire a write and proceed
                locks[name].Wlock.acquire()
                locks[name].Wlock_client = client_id
                print('read locked by self, can write')
                self.tentative_accounts[name] = value
                return "OK"
            
            # if read by others    
            if (client_id not in locks[name].Rlock_clients): #read locked by others
                
                #check if this acuiqre will induce a deadlock:
                if check_deadlock(client_id,name, wait_for_dict,locks):
                    print('Deadlock action, abort {}'.format(client_id))
                    return 'DEAD'
                #Update wait_for_list, 'client_id' needs to wait for read lock on 'name' 
                wait_for_dict[client_id] = name 
                
                locks[name].Rlock.acquire() # wait until read is finished
                locks[name].Wlock.acquire()
                
                #remove the client_id from wait_for_dict
                wait_for_dict.pop(client_id,None)
                
                #record the client for the lock
                locks[name].Wlock_client = client_id
                locks[name].addClients_toRlock(client_id)
            
                self.tentative_accounts[name] = value
                return "OK"
            
            if len(locks[name].Rlock_clients) >1 and (client_id in locks[name].Rlock_clients):
                #read locked by self and others. In this case, others may release lock after self request write,
                #which means the read lock might still on, but self is locking it. should allow write in this case.
                if check_deadlock(client_id,name, wait_for_dict,locks):
                    print('Deadlock action, abort {}'.format(client_id))
                    return 'DEAD'
                #before enter this while loop, check if this while loop will induce dead lock
                wait_for_dict[client_id] = name
                #check(should exclude itself)
                
                while len(locks[name].Rlock_clients) >1:
                    pass
                
                wait_for_dict.pop(client_id,None)
                #if only one left, check
                if client_id in locks[name].Rlock_clients:
                    #can do write:
                    locks[name].Wlock.acquire()
                    locks[name].Wlock_client = client_id
                    self.tentative_accounts[name] = value
                    return "OK"
                else:
                    return 'error, need CHECK'
                
        #Condition 4: both read and write lock exist
        if (locks[name].Wlock.locked()) and (locks[name].Rlock.locked()):
            #check if locked by self:
            if (len(locks[name].Rlock_clients) ==1 and
                client_id in locks[name].Rlock_clients  and
                locks[name].Wlock_client == client_id):
                print('write and read locked by self, OK')
                self.tentative_accounts[name] = value
                return "OK"
            else:
                #check deadlock
                if check_deadlock(client_id,name, wait_for_dict,locks):
                    print('Deadlock action, abort {}'.format(client_id))
                    return 'DEAD'
                #the client_id needs to wait for a lock on 'name'
                wait_for_dict[client_id] = name
                
                locks[name].Wlock.acquire()
                locks[name].Wlock_client = client_id
                
                wait_for_dict.pop(client_id,None)
                #check if Rlock is locked only by self at this time:
                if len(locks[name].Rlock_clients) ==1 and (client_id in locks[name].Rlock_clients):
                    #if read only locked by self
                    locks[name].addClients_toRlock(client_id)
                    self.tentative_accounts[name] = value
                    return "OK"       
                else:
                    #R lock should be clear
                    locks[name].Rlock.acquire()
                    #record the client for the lock
                    locks[name].addClients_toRlock(client_id)
                    self.tentative_accounts[name] = value
                    return "OK"        

    def write_commit(self,name,value,client_id):
        print('Comitting on {} by client {}'.format(name,client_id))
        #write on server's local account dict
        accounts_server[name] = value
        print("Current server's accounts are:")
        print(accounts_server)
        
        #release locks
        #release write if the write lock is locked by the client id:
        if locks[name].Wlock_client == client_id:
            #release lock
            print('releasing write lock')
            locks[name].Wlock.release()
            #remove client_id
            locks[name].release_Wlock()
            
        #release read if the client_id is the only one holding the read lock
        if client_id in locks[name].Rlock_clients:
            #remove from id list
            locks[name].delClients_fromRlock(client_id)
            #If no other clients hold this read lock, release
            if len(locks[name].Rlock_clients) ==0:
                locks[name].Rlock.release()
        
        return 'COMMIT OK'
    
    
    def get_account_value(self,name,client_id):
        #For GET comand
        if name in self.tentative_accounts:
            #check locks
            # if no write lock and no read lock exist:
            if (not locks[name].Wlock.locked()) and (not locks[name].Rlock.locked()): 
                #acquire lock
                locks[name].Rlock.acquire()
                locks[name].addClients_toRlock(client_id)
                #UPDATE TENTATIVE_ACCOUNTS:
                self.tentative_accounts = accounts_server.copy()
                return self.tentative_accounts[name]
            
            #if no write lock and only read lock:
            #can share read lock
            if (not locks[name].Wlock.locked()) and (locks[name].Rlock.locked()):
                locks[name].addClients_toRlock(client_id)
                return self.tentative_accounts[name]
            
            # write lock exists
            if (locks[name].Wlock.locked()):
                #check deadlock
                if check_deadlock(client_id,name, wait_for_dict,locks):
                    print('Deadlock action, abort {}'.format(client_id))
                    return 'DEAD'
                #the client_id needs to wait for a lock on 'name'
                wait_for_dict[client_id] = name

                locks[name].Rlock.acquire()
                locks[name].addClients_toRlock(client_id)
                wait_for_dict.pop(client_id,None)
                #UPDATE TENTATIVE_ACCOUNTS:
                self.tentative_accounts = accounts_server.copy()
                return self.tentative_accounts[name]
            
        else:#no comitted data found
            return 'NOT FOUND'
    
    def releaseReadLock(self,client_id):
        #release all read locks by this client_id
        for key in locks:
            if client_id in locks[key].Rlock_clients:
                print('read lock released-1')
                locks[key].delClients_fromRlock(client_id)
                if len(locks[key].Rlock_clients) == 0:
                    locks[key].Rlock.release()
                    print('read lock released')
        return 'SUCCESS'
    
    def releaseWriteLock(self,client_id):
        #release all locks by this client_id
        for key in locks:
            if locks[key].Wlock_client == client_id:
                locks[key].release_Wlock()
                locks[key].Wlock.release()
        #need to release read locks also
        for key in locks:
            if client_id in locks[key].Rlock_clients:
                locks[key].delClients_fromRlock(client_id)
                if len(locks[key].Rlock_clients) == 0:
                    locks[key].Rlock.release()
        return 'SUCCESS'
    
    #def checkDeadLock(self):
        

    @property
    def get_tent_account(self):
        return self.tentative_account
    @property
    def show(self):             
        return self.accounts

class Account(object):
    def __init__(self,name):
        self.name = name
        self.Rlock = Lock()
        self.Wlock = Lock()
        self.Rlock_clients = set()
        self.Wlock_client = None
        
    def addClients_toRlock(self, client_id):
        self.Rlock_clients.add(client_id)
    def delClients_fromRlock(self,client_id):
        self.Rlock_clients.remove(client_id)
    def addClent_toWlock(self,client_id):
        self.Wlock_client = client_id
    def release_Wlock(self):
        self.Wlock_client = None
        


def check_deadlock(client_id,name, wait_for_dict,locks):
    '''
    wait-for graph to check deadlock
    if an action (client_id, name) will induce deadlock, return True
    '''
    print('checking deadlock')
    #print(wait_for_dict)
    #check who hold 'name':
    hold_list = locks[name].Rlock_clients.copy()  #('1','3')
    if client_id in hold_list:
        hold_list.remove(client_id)
    while len(hold_list)>0:
        print(hold_list)
        new_hold_list = hold_list.copy()
        for item in hold_list:
            #check if the item is waiting for tx or not
            if item not in wait_for_dict:
                new_hold_list.remove(item)
            elif item in wait_for_dict:#if this item is waiting for tx
                print('check point1')
                tx = wait_for_dict[item]
                #check the clients holding tx
                new_clients = locks[tx].Rlock_clients
                print('new_clients')
                print(new_clients)
                if client_id in new_clients:
                    return True
                else:
                    new_hold_list.remove(item)
                    #print('hold_list to be updated:'+ str(new_hold_list))
                    new_hold_list = new_hold_list.union(new_clients)
        hold_list = new_hold_list.copy()
        #print('hold list is '+ str(hold_list))
    return False
    
    
    

def test_thread2():
    #DEBUG THREAD
    while True:
        command2 = input()
        if command2 == 'LOCK':
            for key in locks:
                print(key)
                print('Read lock is locked?:'+ str(locks[key].Rlock.locked()))
                print('Hold by:' + str(locks[key].Rlock_clients))
                print('Write lock is locked?'+ str(locks[key].Wlock.locked()))
                print('Hold by:' + str(locks[key].Wlock_client))
        
#t1 = threading.Thread(Pyro4.Daemon.serveSimple({Transaction: 'A'},host=HOST_IP, port=HOST_PORT_1, ns=False, verbose=True))
 


if __name__ =="__main__":
    if len(sys.argv) != 2:
        print("Usage: python36 server.py <server_id> ")
        print('server id must be A,B,C,D,E')
        sys.exit(1)
    server_id = sys.argv[1]
    #
    accounts_server = {}
    #store read and write locks
    locks = {}
    #wait_for_dict for dead lock detection
    wait_for_dict = dict()
    
    #IP ADRRESS is hard-coded
    HOST_IP = "172.22.94.228"
    Port_server = {'A':8001,
                   'B':8002,
                   'C':8003,
                   'D':8004,
                   'E':8005}
    
    t1 = threading.Thread(target = Pyro4.Daemon.serveSimple,
                          args=[{Transaction: server_id}, HOST_IP, Port_server[server_id], None, False, True])
    t2 = threading.Thread(target = test_thread2)

    t2.start()
    t1.start()

    
    
        
    