#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 29 12:52:40 2019

@author: chen
"""

import Pyro4
import threading
import time
import sys

class local_client(object):
    def __init__(self):
        #store previous commands
        self.commands = []
        #store
        
class tent_transaction_client(object):
    def __init__(self):
        self.commands = []
        self.write_history = []
        self.read_history = []
    def update_write_history(self,server,name,value):
        self.write_history.append([server,name,value])
    def update_read_history(self,server,name):
        self.read_history.append([server,name])


#tx1 = Pyro4.Proxy('PYRO:A@' + IP_server + ':' + str(Port_server))
#print(tx1.set_Rlock())   # call method normally
def aborttx(tent_tx,IP_server,Port_server,client_id):
    #release write locks
    for write_log in tent_tx.write_history:
        server,obj,value = write_log
        #connect to corresponding server
        with Pyro4.Proxy('PYRO:' + server +'@' + IP_server + ':' + str(Port_server[server])) as tx:
            #release
            tx.releaseWriteLock(client_id)
    #release read locks
    for read_log in tent_tx.read_history:
        server,obj = read_log
        #connect to corresponding server
        with Pyro4.Proxy('PYRO:' + server +'@' + IP_server + ':' + str(Port_server[server])) as tx:
            #release
            tx.releaseReadLock(client_id)


def main():
    while True:
        command_ = input()
        if command_ != 'BEGIN':
            print('you need to start a transaction with BEGIN')
            continue
    
        if command_ == 'BEGIN':
            
            #initiate a tentative transaction
            tent_tx = tent_transaction_client() 
            while True:
                command = input()
                action = command.split()[0]
                
                if action == 'SET':
                    action, server_obj, value = command.split()
                    server, obj = server_obj.split('.')
                    value = int(value)
                    
                    #also need to backup in local client
                    tent_tx.update_write_history(server,obj,value)
                    
                    #call RMI
                    with Pyro4.Proxy('PYRO:' + server +'@' + IP_server + ':' + str(Port_server[server])) as tx:
                        result = tx.create_tent_account(obj,value,client_id)
                        if result == 'DEAD':
                            aborttx(tent_tx,IP_server,Port_server,client_id)
                            print('The above command will cause a deadlock!')
                            print('ABORTED')
                            break
                        print(result)
                    continue
                
                if action == 'GET':
                    action, server_obj = command.split()
                    server, obj = server_obj.split('.')
                    #backup in local client
                    tent_tx.update_read_history(server,obj)
                    #check previous write history:
                    return_value = None
                    for write_log in tent_tx.write_history: #look from the most recent one
                        if server == write_log[0]:
                            if obj == write_log[1]:
                                return_value = write_log[2]
                                
                                #print(return_value)
                    if return_value != None:
                        print(return_value)
                        
                    if return_value == None: #if not found in previous commands
                        #call RMI
                        with Pyro4.Proxy('PYRO:' + server +'@' + IP_server + ':' + str(Port_server[server])) as tx:
                            get_result = tx.get_account_value(obj,client_id)
                            if get_result == 'DEAD':
                                aborttx(tent_tx,IP_server,Port_server,client_id)
                                print('The above command will cause a deadlock!')
                                print('ABORTED')
                                break
                            print(get_result)
                            if get_result == 'NOT FOUND':
                                break
                    continue
                
                if action == 'COMMIT':
                    for write_log in tent_tx.write_history:
                        server,obj,value = write_log
                        #connect to corresponding server
                        with Pyro4.Proxy('PYRO:' + server +'@' + IP_server + ':' + str(Port_server[server])) as tx:
                            #write
                            tx.write_commit(obj,value,client_id)
                    #release read locks
                    for read_log in tent_tx.read_history:
                        server,obj = read_log
                        #connect to corresponding server
                        with Pyro4.Proxy('PYRO:' + server +'@' + IP_server + ':' + str(Port_server[server])) as tx:
                            #write
                            tx.releaseReadLock(client_id)
                    print('COMMIT OK')
                    
                    break
                    #release locks: if no write occuerd in the tx.
                    
                    
                if action == "ABORT":
                    #release write locks
                    aborttx(tent_tx,IP_server,Port_server,client_id)
                    #for write_log in tent_tx.write_history:
                    #    server,obj,value = write_log
                    #    #connect to corresponding server
                    #    with Pyro4.Proxy('PYRO:' + server +'@' + IP_server + ':' + str(Port_server[server])) as tx:
                            #release
                    #        tx.releaseWriteLock(client_id)
                    #release read locks
                    #for read_log in tent_tx.read_history:
                    #    server,obj = read_log
                    #    #connect to corresponding server
                    #    with Pyro4.Proxy('PYRO:' + server +'@' + IP_server + ':' + str(Port_server[server])) as tx:
                    #        #release
                    #        tx.releaseReadLock(client_id)
                    print('ABORTED')
                    break
                
if __name__ =='__main__':
    
    if len(sys.argv) != 2:
        print("Usage: python36 client.py <client_id>")
        sys.exit(1)
    IP_server = "172.22.94.228"
    #Port_server = 8001 
    Port_server = {'A':8001,
                   'B':8002,
                   'C':8003,
                   'D':8004,
                   'E':8005}
    
    #client_id = '1'
    client_id = sys.argv[1]
    t1 = threading.Thread(target = main)
    t1.start()
                
                
        